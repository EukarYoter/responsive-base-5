var breadcrumb = {
    init: function () {
        if (!J.checker.isStage){
            J.api.startpage.list(breadcrumb.render, null, true, 30);
        }
    },
    render: function (data) {
        if (data.length) {
            for (var key in data){
                if (data[key]["IsActive"]){
                    var pageName = data[key]["PageName"];
                    $("#path-nav").prepend('<a class="breadcrumb-link" href="/">'+ pageName +'</a><span class="breadcrumb-spacer"> &gt; </span>');
                }
            }
        }
    }
};

J.pages.addToQueue("all-pages", breadcrumb.init);
