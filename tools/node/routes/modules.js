//
//  GET /modules
//
exports.index = function (req, res, next) {
    var allModules = createFileList("core/modules");
    var activeModules = getActiveModules();
    modules = createModuleList(allModules, activeModules);

    res.render('modules', { title: "Modules", modules: modules, activeModules: activeModules});
};

exports.create = function (req, res, next) {
    var fs = require("fs");
    var moduleName = req.body.name;
    var modulesFolderPath = "core/modules/";
    var path = modulesFolderPath + moduleName;

    var mkdirSync = function (path) {
        try {
            fs.mkdirSync(path);
        } catch(e) {
            console.log(e);
            if ( e.code != 'EEXIST' ) throw e;
            res.send(JSON.stringify({"status": "Folder exists", "error": e}));
        }
    };

    var writeFile = function (path, name, extension, content) {
        fs.writeFile(path + "/" + name + "." + extension, content, function(err) {
            if(err) {
                return console.log(err);
            }
        });
    };

    var packageJsonContent = function (moduleName) {
        var str = '';    
        str += '{\n';
        str += '    "name": "' + moduleName + '",\n';
        str += '    "version": "0.1.0",\n';
        str += '    "prototype": true,\n';
        str += '    "description": "Add description here",\n';
        str += '    "author": {\n';
        str += '        "name": "",\n';
        str += '        "email": ""\n';
        str += '    }\n';
        str += '}\n';
        return str;
    };

    mkdirSync(path);
    mkdirSync(path + "/js");
    writeFile(path + "/js", moduleName, "js", "");
    mkdirSync(path + "/scss");
    writeFile(path + "/scss", "_" + moduleName, "scss", "");
    mkdirSync(path + "/views");
    writeFile(path + "/views", moduleName, "hbs", "");
    writeFile(path + "/", "readme", "md", "Add content here");
    writeFile(path + "/", "package", "json", packageJsonContent(moduleName));

    res.end(JSON.stringify({"success":"Folder created", "status": 200 }));
};
